package com.iantsu.projectm.network;

import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.iantsu.projectm.AppController;
import com.iantsu.projectm.data.model.Item;
import com.iantsu.projectm.helper.Constants;

import java.util.List;

/**
 * Project : ProjectM
 * User    : iantsu
 * Date    : 09/05/2016
 */
public class Data {
    private static final String TAG = Data.class.getName();

    public static void getFavoriteItems(final List<Item> list, final RecyclerView.Adapter adapter) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.FIREBASE_ROOT + Constants.USER_FOLDER
                + "/" +  AppController.getInstance().getUid()
                + Constants.FAVORITE_FOLDER);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Item item = snapshot.getValue(Item.class);
                    list.add(item);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void getLaterItems(final List<Item> list, final RecyclerView.Adapter adapter) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.FIREBASE_ROOT + Constants.USER_FOLDER
                + "/" +  AppController.getInstance().getUid()
                + Constants.LATER_FOLDER);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Item item = snapshot.getValue(Item.class);
                    list.add(item);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "onCancelled: " + databaseError.getMessage());

            }
        });
    }
}
