package com.iantsu.projectm;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.facebook.FacebookSdk;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.iantsu.projectm.helper.Constants;

public class AppController extends Application {
    public static final String TAG = AppController.class
            .getSimpleName();


    private static AppController mInstance;
    private DatabaseReference ref;
    private long index;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        FacebookSdk.sdkInitialize(this);

        ref = FirebaseDatabase.getInstance().getReference("data/index");
        ref.keepSynced(true);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                index = (long) dataSnapshot.getValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public SharedPreferences getPref(int mode) {
        return getSharedPreferences(Constants.PREFERENCES, mode);
    }

    public SharedPreferences getPref() {
        return getSharedPreferences(Constants.PREFERENCES, Context.MODE_PRIVATE);
    }

    public String getUsername() {
        return getPref().getString(Constants.USERNAME, "");
    }

    /**
     *
     * @return Current UID, or return default anonymous uid
     */
    public String getUid() {
        return getPref().getString(Constants.UID, Constants.UID_ANONYMOUS);
    }


    public long getTotalRecipe() {
        return index;
    }
}