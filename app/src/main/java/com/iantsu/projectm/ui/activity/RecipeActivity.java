package com.iantsu.projectm.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.iantsu.projectm.AppController;
import com.iantsu.projectm.R;
import com.iantsu.projectm.data.model.DetailRecipe;
import com.iantsu.projectm.data.model.Item;
import com.iantsu.projectm.helper.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipeActivity extends AppCompatActivity {

    @BindView(R.id.collapsingLayout) CollapsingToolbarLayout collapsingLayout;
    @BindView(R.id.panelImage) ImageView panelImage;
    @BindView(R.id.app_bar) AppBarLayout appBarLayout;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.fab) FloatingActionButton fab;
    @BindView(R.id.type) TextView tvType;

    @BindView(R.id.ingredients) TableLayout tbIngredients;
    @BindView(R.id.prior) TextView tvPrior;
    @BindView(R.id.mjob) TextView tvMjob;
    @BindView(R.id.use) TextView tvUse;
    @BindView(R.id.tips) TextView tvTips;

    @BindView(R.id.progressbar) ProgressBar progressBar;
    @BindView(R.id.layout_recipe) LinearLayout recipeLayout;

    private static final String TAG = RecipeActivity.class.getName();


    private ValueEventListener recipeListener;
    private List<DetailRecipe> list;
    private DatabaseReference ref;


    Item homepageItem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        homepageItem = (Item) getIntent().getExtras().getParcelable("data");
        collapsingLayout.setTitle(homepageItem.getName());
        tvType.setText(homepageItem.getType());


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Add favorite here
                String uid = AppController.getInstance().getUid();

                if (!uid.equals(Constants.UID_ANONYMOUS)) {
                    final DatabaseReference firebase = FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.FIREBASE_ROOT + Constants.USER_FOLDER
                            + "/" + uid + Constants.FAVORITE_FOLDER);

                    firebase.orderByChild("url").equalTo(homepageItem.getUrl())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.getChildrenCount() == 0) {
                                        firebase.push().setValue(homepageItem);
                                        Toast.makeText(RecipeActivity.this, R.string.added_successful, Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(RecipeActivity.this,
                                                "Món ăn đã tồn tại trong danh sách của bạn", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                } else {
                    Toast.makeText(RecipeActivity.this, getString(R.string.require_signin), Toast.LENGTH_SHORT).show();
                }
            }
        });

        Glide.with(this)
                .load(homepageItem.getThumbnail())
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .into(panelImage);


        list = new ArrayList<>();


        ref = FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.FIREBASE_ROOT+ homepageItem.getUrl());
        // load data
        recipeListener= new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                DetailRecipe recipe = dataSnapshot.getValue(DetailRecipe.class);

                tvPrior.setText(recipe.getPrior());
                tvMjob.setText(recipe.getMjob());
                tvUse.setText(recipe.getUse());
                tvTips.setText(recipe.getTips());


                String[] values = (String[]) recipe.getIngredients().values().toArray(new String[recipe.getIngredients().values().size()]);
                String[] keys = (String[]) recipe.getIngredients().keySet().toArray(new String[recipe.getIngredients().keySet().size()]);

                for (int i = 0; i < values.length; i++) {
                    LinearLayout layout = new LinearLayout(RecipeActivity.this);
                    layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT
                            , LinearLayout.LayoutParams.WRAP_CONTENT));

                    TextView tvKey = new TextView(RecipeActivity.this);
                    tvKey.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT
                            , 4f));
                    tvKey.setText(keys[i]);

                    TextView tvValue = new TextView(RecipeActivity.this);
                    tvValue.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT
                            , 2f));
                    tvValue.setText(values[i]);

                    layout.addView(tvKey);
                    layout.addView(tvValue);
                    tbIngredients.addView(layout);
                }

                progressBar.setVisibility(View.GONE);
                recipeLayout.setVisibility(View.VISIBLE);
                appBarLayout.setExpanded(true);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        getData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.menu_recipe, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.later :
                String uid = AppController.getInstance().getUid();

                if (!uid.equals(Constants.UID_ANONYMOUS)) {
                    final DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.FIREBASE_ROOT + Constants.USER_FOLDER + "/" + uid
                        + Constants.LATER_FOLDER);

                    ref.orderByChild("url").equalTo(homepageItem.getUrl())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.getChildrenCount() == 0) {
                                        Log.d(TAG, "onDataChange: " + dataSnapshot.getChildrenCount());
                                        ref.push().setValue(homepageItem);
                                        Toast.makeText(RecipeActivity.this, "Đã thêm vào danh sách làm sau !"
                                                , Toast.LENGTH_SHORT).show();
                                    }
                                    else {
                                        Toast.makeText(RecipeActivity.this,
                                                "Món ăn đã tồn tại trong danh sách làm sau", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                } else {
                    Toast.makeText(RecipeActivity.this, R.string.require_signin, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Nhậu cùng với " + getString(R.string.app_name) + " nào anh em ơi");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);

                break;
        }

        return false;
    }

    public void getData() {
        ref.addValueEventListener(recipeListener);
    }

    @Override
    protected void onPause() {
        super.onPause();

        ref.removeEventListener(recipeListener);
    }
}
