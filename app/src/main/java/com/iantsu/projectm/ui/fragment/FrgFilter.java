package com.iantsu.projectm.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.iantsu.projectm.R;
import com.iantsu.projectm.data.adapter.MainAdapter;
import com.iantsu.projectm.data.model.Category;
import com.iantsu.projectm.data.model.Item;
import com.iantsu.projectm.helper.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Project : ProjectM
 * User    : iantsu
 * Date    : 19/05/2016
 */
public class FrgFilter extends Fragment {
    public static FrgFilter newInstance(Category category, String searchText) {
        FrgFilter frg = new FrgFilter();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", category);
        frg.searchText = searchText;
        frg.setArguments(bundle);

        return frg;
    }

    private Category category;
    private String searchText;
    private DatabaseReference ref;
    private ValueEventListener categoryListener;
    private ValueEventListener searchListener;
    private MainAdapter adapter;
    private List<Item> list;

    @BindView(R.id.list) RecyclerView recyclerView;
    @BindView(R.id.progressbar)  ProgressBar progressbar;
    @BindView(R.id.empty) TextView tvEmpty;


    public FrgFilter() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        list = new ArrayList<>();
        category = getArguments().getParcelable("data");


        ref = FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.FIREBASE_ROOT + Constants.RECENT_FOLDER);

        categoryListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Item item = snapshot.getValue(Item.class);
                    list.add(item);

                    adapter.notifyDataSetChanged();
                }

                recyclerView.setVisibility(View.VISIBLE);
                progressbar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        searchListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Item item = snapshot.getValue(Item.class);

                    if (item.getName().toLowerCase().contains(searchText.toLowerCase())) {
                        list.add(item);
                    }

                    adapter.notifyDataSetChanged();
                }

                progressbar.setVisibility(View.GONE);

                if (list.size() == 0) {
                    tvEmpty.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
                else
                    recyclerView.setVisibility(View.VISIBLE);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_filter, container, false);
        ButterKnife.bind(this, v);

        GridLayoutManager glm = new GridLayoutManager(getContext(), 2);
        glm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? 2 : 1;
            }
        });
        recyclerView.setLayoutManager(glm);
        adapter = new MainAdapter(getContext(), list, recyclerView);
        recyclerView.setAdapter(adapter);

        if (searchText == null)
            ref.orderByChild("type").equalTo(category.getName())
                .addValueEventListener(categoryListener);
        else
            ref.addValueEventListener(searchListener);



        return v;
    }

    @Override
    public void onPause() {
        super.onPause();

        ref.removeEventListener(categoryListener);
    }
}
