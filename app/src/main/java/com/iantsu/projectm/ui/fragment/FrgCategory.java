
package com.iantsu.projectm.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.iantsu.projectm.R;
import com.iantsu.projectm.data.adapter.CategoryAdapter;
import com.iantsu.projectm.data.model.Category;
import com.iantsu.projectm.helper.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FrgCategory extends Fragment {

    private static String TAG = FrgCategory.class.getName();

    @BindView(R.id.list) RecyclerView recyclerView;
    @BindView(R.id.progressbar) ProgressBar progressBar;

    private DatabaseReference ref;
    private ValueEventListener categoryListener;

    private List<Category> list;
    private CategoryAdapter adapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_category, container, false);
        ButterKnife.bind(this, v);

        list = new ArrayList<>();
        adapter = new CategoryAdapter(getContext(), list);

        ref = FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.FIREBASE_ROOT + Constants.CATEGORY);

        categoryListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Category category = (Category) snapshot.getValue(Category.class);
                    list.add(category);
                }

                adapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        ref.orderByKey().addValueEventListener(categoryListener);

        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(llm);

        recyclerView.setAdapter(adapter);

        return v;
    }

    @Override
    public void onPause() {
        super.onPause();

        ref.removeEventListener(categoryListener);
    }
}
