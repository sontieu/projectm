package com.iantsu.projectm.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.iantsu.projectm.R;
import com.iantsu.projectm.data.adapter.UserDataAdapter;
import com.iantsu.projectm.data.model.Item;
import com.iantsu.projectm.helper.Constants;
import com.iantsu.projectm.helper.SwipeTouchHelper;
import com.iantsu.projectm.network.Data;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteAcitivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.list) RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);

        List<Item> list = new ArrayList<>();
        UserDataAdapter adapter = new UserDataAdapter(this, list);
        recyclerView.setAdapter(adapter);

        ItemTouchHelper.Callback callback = new SwipeTouchHelper(adapter, Constants.Profile.FAVORITE);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(recyclerView);


        Data.getFavoriteItems(list, adapter);

        // TODO: swipe to delete
    }

}
