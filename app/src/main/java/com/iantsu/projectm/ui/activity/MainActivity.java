package com.iantsu.projectm.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.iantsu.projectm.R;
import com.iantsu.projectm.data.model.Category;
import com.iantsu.projectm.helper.Constants;
import com.iantsu.projectm.ui.fragment.FrgFilter;
import com.iantsu.projectm.ui.fragment.FrgHomepage;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements FrgHomepage.HomeListener,
        SearchView.OnQueryTextListener
{

    private static String TAG = MainActivity.class.getName();

    private FragmentManager fragmentManager;
    private String username;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tabs) TabLayout tabLayout;
    @BindView(R.id.title) TextView tvTitle;
    private MenuItem search;

    private static String HOMEPAGE = "homepage";
    private static String FILTER = "filter";
    private static String INGREDIENT = "ingredient";

    private FrgHomepage frgHomepage;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case Constants.RECIPE_CATEGORY:
                    Category cat = intent.getExtras().getParcelable("data");
                    tabLayout.setVisibility(View.GONE);
                    tvTitle.setText("Category :" + cat.getName());

                    search.setVisible(false);

                    fragmentManager.beginTransaction()
                            .replace(R.id.mainpage,
                                    FrgFilter.newInstance(cat, null), FILTER)
                            .addToBackStack(FILTER)
                            .commit();
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        fragmentManager = getSupportFragmentManager();
        frgHomepage = FrgHomepage.newInstance(this);
        fragmentManager.beginTransaction()
                .replace(R.id.mainpage, frgHomepage, HOMEPAGE)
                .commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,
                new IntentFilter(Constants.RECIPE_CATEGORY));
    }

    @Override
    protected void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            if (fragmentManager.findFragmentByTag(HOMEPAGE) != null) {
                tabLayout.setVisibility(View.VISIBLE);
                tvTitle.setText(getString(R.string.app_name));

                search.setVisible(true);
            }

            super.onBackPressed();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) search.getActionView();
        searchView.setQueryHint("món, chuyên mục (có dấu)");

        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void attachViewPager(ViewPager viewPager) {
        tabLayout.setupWithViewPager(viewPager);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        tabLayout.setVisibility(View.GONE);
        tvTitle.setText("Search :" + query);

        FragmentTransaction tran = fragmentManager.beginTransaction()
                .replace(R.id.mainpage,
                        FrgFilter.newInstance(null, query), FILTER);
        if (fragmentManager.findFragmentByTag(FILTER) == null)
                tran.addToBackStack(FILTER);

        tran.commit();
        search.collapseActionView();

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
