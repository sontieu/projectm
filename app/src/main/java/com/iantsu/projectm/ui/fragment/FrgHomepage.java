package com.iantsu.projectm.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iantsu.projectm.R;
import com.iantsu.projectm.data.adapter.ViewPagerAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Project : ProjectM
 * User    : iantsu
 * Date    : 07/05/2016
 */
public class FrgHomepage extends Fragment {
    @BindView(R.id.viewpager) ViewPager viewPager;

    private HomeListener homeListener;
    private List<String> titles;
    private List<Fragment> list;
    private ViewPagerAdapter adapter;

    private static String TAG = FrgHomepage.class.getName();

    public static FrgHomepage newInstance(HomeListener listener) {
        FrgHomepage frg = new FrgHomepage();

        frg.homeListener = listener;

        return frg;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        titles = Arrays.asList("Cập nhật", "Chuyên mục", "Nguyên liệu");
        list = new ArrayList<>();
        list.add(new FrgRecent());
        list.add(new FrgCategory());
//        list.add(new FrgIngredient());
        if (adapter == null)
            adapter = new ViewPagerAdapter(getChildFragmentManager(), list, titles);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_homepage, container, false);
        ButterKnife.bind(this, v);

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(0);
        
        if (homeListener != null)
            homeListener.attachViewPager(viewPager);

        return v;
    }


    public interface HomeListener {
        void attachViewPager(ViewPager viewPager);
    }

}
