package com.iantsu.projectm.ui.view;

import android.content.Context;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;

/**
 * DAILY HUNT NEWS
 * Created by : Android Studio 1.5.1
 */
public class EmailTextWatcher implements TextWatcher{
    public interface EmailCheckListener {
        public void validateEmail(CharSequence s);
    }

    private Context context;
    private EmailCheckListener listener;

    public EmailTextWatcher(Context context, EmailCheckListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (listener != null)
            listener.validateEmail(s);
    }
}
