package com.iantsu.projectm.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.iantsu.projectm.AppController;
import com.iantsu.projectm.R;
import com.iantsu.projectm.data.adapter.MainAdapter;
import com.iantsu.projectm.data.model.Item;
import com.iantsu.projectm.helper.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FrgRecent extends Fragment implements MainAdapter.OnLoadMore {
    @BindView(R.id.main_items) RecyclerView recyclerView;
    @BindView(R.id.progressbar) ProgressBar progressBar;

    private static String TAG = FrgRecent.class.getName();

    private long item_loaded;

    private List<Item> list;
    private MainAdapter adapter;
    private DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.FIREBASE_ROOT + "/" + Constants.RECENT_FOLDER);
    private ValueEventListener recentListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_recent, container, false);

        ButterKnife.bind(this, v);


        GridLayoutManager gridManager = new GridLayoutManager(getContext(), 2);
        gridManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? 2 : 1;
            }
        });
        recyclerView.setLayoutManager(gridManager);

        list = new ArrayList<>();
        adapter = new MainAdapter(getContext(), list, recyclerView);
        adapter.setLoadmoreListener(this);
        recyclerView.setAdapter(adapter);

        item_loaded = AppController.getInstance().getTotalRecipe();
        Log.d(TAG, "onCreateView: " + item_loaded);
        item_loaded = getData(list);

        return v;
    }

    public long getData(final List<Item> list) {

        recentListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Item> temp = new ArrayList<>();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Item item = snapshot.getValue(Item.class);
                    Log.d(TAG, "onDataChange: " + item.getIndex());
                    temp.add(0, item);
                }

                list.addAll(temp);
                adapter.notifyDataSetChanged();

                recyclerView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        ref.orderByChild("index").startAt(item_loaded - Constants.ITEM_LOAD)
                .endAt(item_loaded).addValueEventListener(recentListener);

        long new_loaded = item_loaded - Constants.ITEM_LOAD - 1;

        if (new_loaded < 0)
            adapter.setLoadmoreListener(null);

        return new_loaded;
    }

    @Override
    public void onPause() {
        super.onPause();

        ref.removeEventListener(recentListener);
    }

    @Override
    public void onLoadMore() {
        Log.d(TAG, "onLoadMore: ");
        item_loaded = getData(list);
    }
}
