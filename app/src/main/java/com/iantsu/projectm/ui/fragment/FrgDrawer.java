package com.iantsu.projectm.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.iantsu.projectm.AppController;
import com.iantsu.projectm.R;
import com.iantsu.projectm.ui.activity.FavoriteAcitivity;
import com.iantsu.projectm.ui.activity.LaterActivity;
import com.iantsu.projectm.ui.activity.LoginActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Project : ProjectM
 * User    : iantsu
 * Date    : 07/05/2016
 */
public class FrgDrawer extends Fragment {
    @BindView(R.id.tvUser) TextView tvUser;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_drawer, container, false);
        ButterKnife.bind(this, v);

        tvUser.setText("Hi " + AppController.getInstance().getUsername());

        return v;
    }

    @OnClick(R.id.logout)
    public void logout() {
        FirebaseAuth.getInstance().signOut();

        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        getActivity().startActivity(intent);
    }

    @OnClick(R.id.favorite)
    public void drawerFavorite() {
        Intent intent = new Intent(getContext(), FavoriteAcitivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cook_later)
    public void drawerLater() {
        Intent intent = new Intent(getContext(), LaterActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.information)
    public void inform() {

    }

    @OnClick(R.id.settings)
    public void settings() {

    }

}
