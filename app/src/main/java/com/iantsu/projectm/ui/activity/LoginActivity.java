package com.iantsu.projectm.ui.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.iantsu.projectm.AppController;
import com.iantsu.projectm.R;
import com.iantsu.projectm.helper.Constants;
import com.iantsu.projectm.ui.view.EmailTextWatcher;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.facebook_login) LoginButton btnFacebook;
    @BindView(R.id.login) Button btnLogin;
    @BindView(R.id.etEmail) TextInputEditText etEmail;
    @BindView(R.id.etPassword) TextInputEditText etPassword;
    @BindView(R.id.inputEmail) TextInputLayout inputEmail;
    @BindView(R.id.inputPassword) TextInputLayout inputPassword;
    @BindView(R.id.skip) TextView tvSkip;

    private static String TAG = LoginActivity.class.getSimpleName();
    private DatabaseReference ref;
    private CallbackManager fbCallback;
    private String username;
    private FirebaseAuth.AuthStateListener authListener;
    public static final int GOOGLE_SIGNIN_CODE = 5540;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        ref = FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.FIREBASE_ROOT);
        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        // Facebook Login
        btnFacebook.setReadPermissions("email");
        fbCallback = CallbackManager.Factory.create();
        btnFacebook.registerCallback(fbCallback, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                handleFacebookLogin(loginResult);
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "Facebook.onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "onError: " + error.getMessage());
            }
        });

        // Validating email
        etEmail.addTextChangedListener(new EmailTextWatcher(this, new EmailTextWatcher.EmailCheckListener() {
            @Override
            public void validateEmail(CharSequence s) {
                if (!Patterns.EMAIL_ADDRESS.matcher(s).find()) {
                    inputEmail.setErrorEnabled(true);
                    inputEmail.setError(getString(R.string.error_email_format));
                } else {
                    inputEmail.setErrorEnabled(false);
                }
            }
        }));

    }

    private void handleFacebookLogin(final LoginResult loginResult) {
        Log.d(TAG, "onSuccess: ");
        if (loginResult.getAccessToken() != null) {
            final AuthCredential credential = FacebookAuthProvider
                    .getCredential(loginResult.getAccessToken().getToken());

            FirebaseAuth.getInstance().signInWithCredential(credential)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "onAuthenticated: Facebook loged in");
                                Map<String, String> map = new HashMap<String, String>();
                                map.put("provider", credential.getProvider());
                                map.put("token", loginResult.getAccessToken().getToken());

                                ref.child("users/" + task.getResult().getUser().getUid() + "/profile").setValue(map);
                                username = loginResult.getAccessToken().getUserId();

                                loginSuccessful();
                            } else {
                                Log.w(TAG, "signInWithCredential", task.getException());
                                Toast.makeText(LoginActivity.this, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    @OnClick(R.id.skip)
    public void skip() {
//        TODO : Anonymous login
        FirebaseAuth.getInstance().signInWithEmailAndPassword(Constants.USER_ANONYMOUS, Constants.PASS_ANONyMOUS)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "onAuthenticated: OK");
                            Map<String, String> map = new HashMap<String, String>();
                            map.put("provider", "anonymous");

                            ref.child("users").child(task.getResult().getUser().getUid() + "/profile").setValue(map);

                            loginSuccessful();
                        } else {
                            Log.w(TAG, "signInWithAnonymousEmail", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @OnClick(R.id.login)
    public void signIn() {
        Log.d(TAG, "signIn: " + etEmail.getText() + " " + etPassword.getText());

        FirebaseAuth.getInstance().signInWithEmailAndPassword(etEmail.getText().toString(), etPassword.getText().toString())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "onAuthenticated: " + task.getResult().getUser().getUid());
                            username = etEmail.getText().toString();

                            Map<String, String> map = new HashMap<String, String>();
                            map.put("provider", "email");
                            map.put("user", etEmail.getText().toString());

                            ref.child("users").child(task.getResult().getUser().getUid() + "/profile").setValue(map);

                            loginSuccessful();
                        } else {
                            Log.w(TAG, "signInWithEmail", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @OnClick(R.id.google_signin)
    public void googleLogin() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.default_web_client_id))
                .build();
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Log.d(TAG, "onConnectionFailed: " + connectionResult.getErrorMessage());
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, GOOGLE_SIGNIN_CODE);
    }

    @OnClick(R.id.signup)
    public void signup() {
        boolean emailError = !Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).find();
        boolean passwordError = etPassword.getText().toString().isEmpty();

        if (emailError)
            requestFocus(inputEmail);
        else if (passwordError)
            requestFocus(inputPassword);
        else {
            // no error
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(etEmail.getText().toString(),
                    etPassword.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Map<String, String> map = new HashMap<String, String>();
                                map.put("provider", "password");
                                map.put("user", etEmail.getText().toString());

                                ref.child("users").child(task.getResult().getUser().getUid()).setValue(map);
                                username = etEmail.getText().toString();
                                loginSuccessful();
                            } else {
                                if (!task.isSuccessful()) {
                                    Log.d(TAG, "onComplete: " + task.getException());
                                    Toast.makeText(LoginActivity.this, "Authentication failed.",
                                            Toast.LENGTH_SHORT).show();
                                }

                            }
                        }
                    });
            }
    }

    public void requestFocus(View v) {
        if (v.requestFocus())
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GOOGLE_SIGNIN_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            try {
                handleSignInResult(result);
            } catch (GoogleAuthException | IOException e) {
                e.printStackTrace();
            }
        } else
            fbCallback.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(GoogleSignInResult result) throws GoogleAuthException, IOException {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess() + " " + result.getStatus());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            final GoogleSignInAccount acct = result.getSignInAccount();
            Log.d(TAG, "handleSignInResult: " + acct.getIdToken());
            AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);

            FirebaseAuth.getInstance().signInWithCredential(credential)
                    .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "onAuthenticated: OK");
                                Map<String, String> map = new HashMap<String, String>();
                                map.put("provider", "google");
                                map.put("token", task.getResult().getUser().getToken(false).getResult().getToken());
                                ref.child("users").child(task.getResult().getUser().getUid() + "/profile").setValue(map);

                                username = task.getResult().getUser().getEmail();
                                loginSuccessful();
                            } else {
                                Log.d(TAG, "onComplete: " + task.getException());
                                Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

        } else {
            // Signed out, show unauthenticated UI.
            Log.d(TAG, "handleSignInResult: ");
        }



    }



    public void loginSuccessful() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);

        AppController.getInstance()
                .getPref().edit().putString(Constants.USERNAME, username)
                .apply();
        AppController.getInstance()
                .getPref().edit().putString(Constants.UID, FirebaseAuth.getInstance().getCurrentUser().getUid())
                .apply();

        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }


    @Override
    public void onStart() {
        super.onStart();
        FirebaseAuth.getInstance().addAuthStateListener(authListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null) {
            FirebaseAuth.getInstance().removeAuthStateListener(authListener);
        }
    }
}
