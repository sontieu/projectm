package com.iantsu.projectm.helper;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.iantsu.projectm.data.adapter.UserDataAdapter;

/**
 * Project : ProjectM
 * User    : iantsu
 * Date    : 21/05/2016
 */
public class SwipeTouchHelper extends ItemTouchHelper.SimpleCallback {
    private UserDataAdapter adapter;
    private Constants.Profile profile;

    public SwipeTouchHelper(UserDataAdapter adapter, Constants.Profile profile) {
        super(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) ;
        this.adapter = adapter;

        this.profile = profile;
    }


    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        adapter.remove(viewHolder.getAdapterPosition(), profile);
    }
}
