package com.iantsu.projectm.helper;

/**
 * Project M
 * Created by : Android Studio 1.5.1
 * Create at : 23/04/2016
 */
public class Constants  {
    public static final String FIREBASE_ROOT = "https://monan4phuong.firebaseio.com";
    public static final String PREFERENCES = "projectm";
    public static final String UID = "uid";
    public static final String USERNAME = "username";
    public static final String RECENT_FOLDER = "/data/recent";
    public static final String USER_FOLDER  = "/users";
    public static final String FAVORITE_FOLDER = "/favorite";
    public static final String LATER_FOLDER = "/later";
    public static final String INDEX = "/data/index";
    public static final String CATEGORY = "/data/category";

    public static final String USER_ANONYMOUS = "anonymous@anonymous.com";
    public static final String PASS_ANONyMOUS = "123456";
    public static final String UID_ANONYMOUS = "4ad840df-ad80-4890-b790-1708e5ad1a9a";

    public static final int ITEM_LOAD = 4;
    public static final int ITEM_PRELOADING = 2;

    public static final String RECIPE_CATEGORY = "com.iantsu.projectm.RECIPE_BY_CATEGORY";

    public enum Profile {
        FAVORITE,
        LATER
    }

}
