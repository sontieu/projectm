package com.iantsu.projectm.data.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.iantsu.projectm.AppController;
import com.iantsu.projectm.R;
import com.iantsu.projectm.data.model.Item;
import com.iantsu.projectm.helper.Constants;
import com.iantsu.projectm.ui.activity.RecipeActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Project : ProjectM
 * User    : iantsu
 * Date    : 08/05/2016
 */
public class MainAdapter extends RecyclerView.Adapter<MainAdapter.RecentViewHolder> {
    private Context context;
    private List<Item> list;
    private RecyclerView recyclerView;

    private static int HOT_DAILY = 0;
    private static int NORMAL = 1;

    public OnLoadMore loadmoreListener;
    private boolean isLoading = false;

    int lastItemVisible, totalItem;


    public void setLoadmoreListener(OnLoadMore loadmoreListener) {
        this.loadmoreListener = loadmoreListener;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public MainAdapter(Context context, List<Item> list, RecyclerView recyclerView) {
        this.context = context;
        this.list = list;
        this.recyclerView = recyclerView;

        final GridLayoutManager llm = (GridLayoutManager) this.recyclerView.getLayoutManager();

        this.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItem = llm.getItemCount();
                lastItemVisible = llm.findLastVisibleItemPosition();

                if (totalItem - Constants.ITEM_PRELOADING <= lastItemVisible) {
                    if (loadmoreListener != null)
                        loadmoreListener.onLoadMore();
                }
            }
        });


    }

    public interface OnLoadMore {
        void onLoadMore();
    }

    public class RecentViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image) ImageView imgView;
        TextView tvType;
        @BindView(R.id.name) TextView tvName;
        @BindView(R.id.description) TextView tvDescription;
        TextView tvAddFavorite;
        View itemView;

        public RecentViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            tvAddFavorite = (TextView) itemView.findViewById(R.id.add_favorite);
            tvType = (TextView) itemView.findViewById(R.id.type);
            this.itemView = itemView;
        }
    }

    @Override
    public RecentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(
                viewType == HOT_DAILY ? R.layout.item_hotdaily : R.layout.item_homepage, parent, false);



        return new RecentViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecentViewHolder holder, int position) {
        final Item item = (Item) list.get(position);


        Glide.with(context)
                .load(item.getThumbnail())
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .into(holder.imgView);
        
        holder.tvName.setText(item.getName());
        holder.tvDescription.setText(item.getDescription());

        if (holder.tvType != null)
            holder.tvType.setText(item.getType());

        if (holder.tvAddFavorite != null)
            holder.tvAddFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String uid = AppController.getInstance().getUid();
                    if (!uid.equals(Constants.UID_ANONYMOUS)) {

                        final DatabaseReference firebase = FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.FIREBASE_ROOT + Constants.USER_FOLDER
                                + "/" + uid + Constants.FAVORITE_FOLDER);

                        firebase.orderByChild("url").equalTo(item.getUrl())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.getChildrenCount() == 0) {
                                            firebase.push().setValue(item);
                                            Toast.makeText(context, R.string.added_successful, Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context,
                                                    "Món ăn đã tồn tại trong danh sách của bạn", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                        });
                    } else {
                        Toast.makeText(context, context.getString(R.string.require_signin), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RecipeActivity.class);
                intent.putExtra("data", item);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? HOT_DAILY : NORMAL;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
