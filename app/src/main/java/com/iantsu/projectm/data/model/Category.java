package com.iantsu.projectm.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Project : ProjectM
 * User    : iantsu
 * Date    : 19/05/2016
 */
public class Category implements Parcelable {
    protected boolean hot;
    protected String image;
    protected String name;

    public Category() {

    }

    public Category(Category category) {
        this.hot = category.hot;
        this.image = category.image;
        this.name = category.name;
    }

    public Category(boolean hot, String image, String name) {
        this.hot = hot;
        this.image = image;
        this.name = name;
    }

    public boolean isHot() {
        return hot;
    }

    public void setHot(boolean hot) {
        this.hot = hot;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.hot ? (byte) 1 : (byte) 0);
        dest.writeString(this.image);
        dest.writeString(this.name);
    }

    protected Category(Parcel in) {
        this.hot = in.readByte() != 0;
        this.image = in.readString();
        this.name = in.readString();
    }

    public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
