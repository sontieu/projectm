package com.iantsu.projectm.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

/**
 * Project : ProjectM
 * User    : iantsu
 * Date    : 13/05/2016
 */
public class DetailRecipe implements Parcelable {
    protected String prior;
    protected String mjob;
    protected String use;
    protected String tips;
    protected Map<String, String> ingredients;

    public DetailRecipe() {

    }

    public DetailRecipe(DetailRecipe item) {
        this.prior = item.prior;
        this.mjob = item.mjob;
        this.use = item.use;
        this.tips = item.tips;
        this.ingredients = item.ingredients;
    }

    public String getPrior() {
        return prior;
    }

    public void setPrior(String prior) {
        this.prior = prior;
    }

    public String getMjob() {
        return mjob;
    }

    public void setMjob(String mjob) {
        this.mjob = mjob;
    }

    public String getUse() {
        return use;
    }

    public void setUse(String use) {
        this.use = use;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public Map<String, String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Map<String, String> ingredients) {
        this.ingredients = ingredients;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.prior);
        dest.writeString(this.mjob);
        dest.writeString(this.use);
        dest.writeString(this.tips);
        dest.writeInt(this.ingredients.size());
        for (Map.Entry<String, String> entry : this.ingredients.entrySet()) {
            dest.writeString(entry.getKey());
            dest.writeString(entry.getValue());
        }
    }

    protected DetailRecipe(Parcel in) {
        this.prior = in.readString();
        this.mjob = in.readString();
        this.use = in.readString();
        this.tips = in.readString();
        int ingredientsSize = in.readInt();
        this.ingredients = new HashMap<String, String>(ingredientsSize);
        for (int i = 0; i < ingredientsSize; i++) {
            String key = in.readString();
            String value = in.readString();
            this.ingredients.put(key, value);
        }
    }

    public static final Parcelable.Creator<DetailRecipe> CREATOR = new Parcelable.Creator<DetailRecipe>() {
        @Override
        public DetailRecipe createFromParcel(Parcel source) {
            return new DetailRecipe(source);
        }

        @Override
        public DetailRecipe[] newArray(int size) {
            return new DetailRecipe[size];
        }
    };
}
