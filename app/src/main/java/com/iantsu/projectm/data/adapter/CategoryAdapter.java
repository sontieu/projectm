package com.iantsu.projectm.data.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.iantsu.projectm.R;
import com.iantsu.projectm.data.model.Category;
import com.iantsu.projectm.helper.Constants;
import com.lid.lib.LabelTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Project : ProjectM
 * User    : iantsu
 * Date    : 19/05/2016
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {
    private Context context;
    private List<Category> list;


    public CategoryAdapter(Context context, List<Category> list) {
        this.context = context;
        this.list= list;
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image) ImageView image;
        @BindView(R.id.name) LabelTextView name;
        View layout;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            layout = itemView;
            ButterKnife.bind(this, itemView);
        }
    }
    public CategoryAdapter.CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_category, parent, false);

        return new CategoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CategoryAdapter.CategoryViewHolder holder, int position) {
        final Category category = list.get(position);


        holder.name.setText(category.getName());
        if (!category.isHot())
            holder.name.setLabelEnable(false);


        Glide.with(context).load(category.getImage())
                .into(holder.image);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Constants.RECIPE_CATEGORY);
                intent.putExtra("data", category);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
