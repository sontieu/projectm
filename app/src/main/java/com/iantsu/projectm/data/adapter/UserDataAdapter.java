package com.iantsu.projectm.data.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.iantsu.projectm.R;
import com.iantsu.projectm.data.model.Item;
import com.iantsu.projectm.helper.Constants;
import com.iantsu.projectm.ui.activity.RecipeActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Project : ProjectM
 * User    : iantsu
 * Date    : 13/05/2016
 */
public class UserDataAdapter extends RecyclerView.Adapter<UserDataAdapter.UserDataHolder> {
    private Context context;
    private List<Item> list;

    public UserDataAdapter(Context context, List<Item> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public UserDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_userdata, parent, false);

        return new UserDataHolder(v);
    }

    @Override
    public void onBindViewHolder(UserDataHolder holder, int position) {
        final Item item = list.get(position);

        holder.name.setText(item.getName());
        holder.type.setText(item.getType());
        holder.description.setText(item.getDescription());

        Glide.with(context)
                .load(item.getThumbnail())
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .into(holder.image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RecipeActivity.class);
                intent.putExtra("data", item);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class UserDataHolder extends RecyclerView.ViewHolder{
        View itemView;
        @BindView(R.id.image) ImageView image;
        @BindView(R.id.name) TextView name;
        @BindView(R.id.type) TextView type;
        @BindView(R.id.description) TextView description;


        public UserDataHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;

            ButterKnife.bind(this, itemView);
        }
    }

    public void remove(int position, Constants.Profile profile) {
        // delete from server
        final Item item = list.get(position);
        final DatabaseReference ref;

        switch (profile) {
            case FAVORITE:
                ref = FirebaseDatabase.getInstance()
                        .getReferenceFromUrl(Constants.FIREBASE_ROOT + Constants.USER_FOLDER + "/"
                                + FirebaseAuth.getInstance().getCurrentUser().getUid() + Constants.FAVORITE_FOLDER);
                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Item snapItem = snapshot.getValue(Item.class);
                            if (snapItem.getUrl().equals(item.getUrl())) {
                                ref.child(snapshot.getKey()).setValue(null);
                                ref.removeEventListener(this);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                break;
            case LATER:
                ref = FirebaseDatabase.getInstance()
                        .getReferenceFromUrl(Constants.FIREBASE_ROOT + Constants.USER_FOLDER + "/"
                                + FirebaseAuth.getInstance().getCurrentUser().getUid() + Constants.LATER_FOLDER);
                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Item snapItem = snapshot.getValue(Item.class);
                            if (snapItem.getUrl().equals(item.getUrl())) {
                                ref.child(snapshot.getKey()).setValue(null);
                                ref.removeEventListener(this);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                break;
        }

        list.remove(position);
        notifyItemRemoved(position);
    }
}
