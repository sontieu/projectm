package com.iantsu.projectm.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Project : ProjectM
 * User    : iantsu
 * Date    : 08/05/2016
 */
public class Item implements Parcelable {
    protected String name;
    protected String description;
    protected String type;
    protected String thumbnail;
    protected String url;
    protected int index;

    public Item() {

    }

    public Item(Item item) {
        this.name = item.name;
        this.description = item.description;
        this.type = item.type;
        this.thumbnail = item.thumbnail;
        this.url = item.url;
        this.index = item.index;
    }

    public Item(String name, String description, String type, String thumbnail, String url, int index) {
        this.name = name;
        this.description = description;
        this.type = type;
        this.thumbnail = thumbnail;
        this.url = url;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.type);
        dest.writeString(this.thumbnail);
        dest.writeString(this.url);
        dest.writeInt(this.index);
    }

    protected Item(Parcel in) {
        this.name = in.readString();
        this.description = in.readString();
        this.type = in.readString();
        this.thumbnail = in.readString();
        this.url = in.readString();
        this.index = in.readInt();
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel source) {
            return new Item(source);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };
}
